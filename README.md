Sale Tax Included
=================

Adiczion's Tryton Module: sale_tax_included
-------------------------------------------

Adds the ability to Tryton to make sales and invoices with product with 
price with tax included.

.. warning::
   Replace the deprecated module : sale_b2bc.

Installing
----------

See INSTALL

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

  Adiczion
  --------
  website: http://www.adiczion.com/
  email: atm@adiczion.net

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  http://bugs.tryton.org/
  http://groups.tryton.org/
  http://wiki.tryton.org/
  irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

Additional information
----------------------

For more information please visit the Tryton web site:

  http://www.tryton.org/
