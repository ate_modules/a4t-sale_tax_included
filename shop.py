# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not, If

__all__ = ['SaleShop']

_PRICE_TYPE = [
    ('both', 'Tax included & Tax excluded'),
    ('t_inc', 'Tax included (only)'),
    ('t_exc', 'Tax excluded (only)'),
    ]


class SaleShop(ModelSQL, ModelView):
    __name__ = 'sale.shop'

    price_type = fields.Selection(_PRICE_TYPE, 'Price type',
        states={
            'required': Bool(Eval('context', {}).get('company', 0)),
            },
        help='Type of price authorized on this shop.')

    @staticmethod
    def default_price_type():
        return 'both'
