#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import test_view, test_depends, \
    POOL, DB_NAME, USER, CONTEXT
from trytond.transaction import Transaction

class SaleTaxIncludedTestCase(unittest.TestCase):
    'Test Sale Tax Included module'

    def setUp(self):
        # Install the module
        trytond.tests.test_tryton.install_module('sale_tax_included')

    def test0005views(self):
        'Test views'
        test_view('sale_tax_included')

    def test0006depends(self):
        'Test depends'
        test_depends()

def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        SaleTaxIncludedTestCase))
    # Add any other tests from this module
    return suite