# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from decimal import Decimal
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not, If
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = [
    'Sale', 'SaleLine',
]

_ZERO = Decimal('0.0')
_PRICE_TYPE = [
    ('t_inc', 'Tax included'),
    ('t_exc', 'Tax excluded'),
    ]
_DEPENDS = ['state', 'shop_price_type', 'party_price_type', 'lines']

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class Sale(Workflow, ModelSQL, ModelView):
    __name__ = 'sale.sale'

    price_type = fields.Selection(_PRICE_TYPE, 'Price type', required=True,
        states={
            'readonly': (Eval('state') != 'draft') \
                | (Eval('party_price_type') != 'both') \
                | (Eval('shop_price_type') != 'both') \
                | (Bool(Eval('lines'))),
        },
        selection_change_with = ['shop', 'party'],
        depends=_DEPENDS,
        help='Type of price of this sale.')
    shop_price_type = fields.Function(fields.Char('Shop price type'),
        'on_change_with_shop_price_type')
    party_price_type = fields.Function(fields.Char('Party price type'),
        'on_change_with_party_price_type')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        if not getattr(cls.lines, 'context', False):
            cls.lines.context = {}
        cls.lines.context.update(
            {'tax_included': Bool(Eval('price_type', 't_exc') == 't_inc')})
        if not cls.party.depends:
            cls.party.depends = []
        if 'shop_price_type' not in cls.party.depends:
            cls.party.depends.append('shop_price_type')
        if not cls.party.domain:
            cls.party.domain = []
        if 'price_type' not in [x[0] for x in cls.party.domain]:
            cls.party.domain.append((
                'price_type',
                'in',
                If(Eval('shop_price_type', {}).contains('both'),
                   ['t_inc', 't_exc', 'both'],
                   [Eval('shop_price_type'), 'both']),
            ))
        cls._error_messages.update({
            'wrong_price_type': ("The price type selected is '%(price_type)s "
                                 "...")
            })

    @classmethod
    def default_price_type(cls):
        result = cls.default_shop_price_type()
        return result if result != 'both' else 't_exc'

    @staticmethod
    def default_shop_price_type():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.price_type if user.shop else 'both'

    @staticmethod
    def default_party_price_type():
        return 'both'

    def _get_price_type(self, shop=None, party=None):
        result = False
        party_price_type = party and party.price_type or None
        shop_price_type = shop and shop.price_type or None

        if party_price_type and party_price_type != 'both':
            result = party_price_type
        if not result:
            if shop_price_type and shop_price_type != 'both':
                result = shop_price_type
        return result

    @fields.depends('price_type', 'shop', 'party')
    def on_change_shop(self):
        res = super(Sale, self).on_change_shop()
        result = self._get_price_type(party=self.party, shop=self.shop)
        if result:
            res['price_type'] = result
        return res

    @fields.depends('price_type', 'shop', 'party')
    def on_change_party(self):
        res = super(Sale, self).on_change_party()
        result = self._get_price_type(party=self.party, shop=self.shop)
        if result:
            res['price_type'] = result
        return res

    @fields.depends('shop')
    def on_change_with_shop_price_type(self, name=None):
        if self.shop:
            return self.shop.price_type
        return 'both'

    @fields.depends('party')
    def on_change_with_party_price_type(self, name=None):
        if self.party:
            return self.party.price_type
        return 'both'

    @fields.depends('price_type')
    def get_tax_context(self):
        res = super(Sale, self).get_tax_context()
        res['tax_included'] = True if self.price_type == 't_inc' else False
        return res

    @fields.depends('price_type')
    def on_change_lines(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        Invoice = pool.get('account.invoice')

        if self.price_type == 't_exc':
            return super(Sale, self).on_change_lines()

        res = {
            'untaxed_amount': Decimal('0.0'),
            'tax_amount': Decimal('0.0'),
            'total_amount': Decimal('0.0'),
            }

        if self.lines:
            taxes = {}
            for line in self.lines:
                if getattr(line, 'type', 'line') != 'line':
                    continue
                res['total_amount'] += line.amount or Decimal(0)
                tax_list = ()
                with Transaction().set_context(self.get_tax_context()):
                    tax_list = Tax.compute(getattr(line, 'taxes', []),
                        line.unit_price or Decimal('0.0'),
                        line.quantity or 0.0)
                for tax in tax_list:
                    key, val = Invoice._compute_tax(tax, 'out_invoice')
                    if self.currency:
                        val['amount'] = self.currency.round(val['amount'])
                    if not key in taxes:
                        taxes[key] = val['amount']
                    else:
                        taxes[key] += val['amount']
            for key in taxes:
                res['tax_amount'] += taxes[key]
        res['untaxed_amount'] = res['total_amount'] - res['tax_amount']
        if self.currency:
            res['total_amount'] = self.currency.round(res['total_amount'])

        return res

    def get_tax_amount(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        Invoice = pool.get('account.invoice')

        if self.price_type == 't_exc':
            return super(Sale, self).get_tax_amount()

        context = self.get_tax_context()
        taxes = {}
        for line in self.lines:
            if line.type != 'line':
                continue
            with Transaction().set_context(context):
                tax_list = Tax.compute(line.taxes, line.unit_price,
                    line.quantity)
            # Round on each line in tax included price to handle rounding error
            for tax in tax_list:
                key, val = Invoice._compute_tax(tax, 'out_invoice')
                if self.currency:
                    val['amount'] = self.currency.round(val['amount'])
                if not key in taxes:
                    taxes[key] = val['amount']
                else:
                    taxes[key] += val['amount']
        return sum((tax for tax in taxes.values()), _ZERO)

    @classmethod
    def get_amount(cls, sales, names):
        untaxed_amount = {}
        tax_amount = {}
        total_amount = {}

        for sale in sales:
            if (sale.state in cls._states_cached
                    and sale.untaxed_amount_cache is not None
                    and sale.tax_amount_cache is not None
                    and sale.total_amount_cache is not None):
                untaxed_amount[sale.id] = sale.untaxed_amount_cache
                tax_amount[sale.id] = sale.tax_amount_cache
                total_amount[sale.id] = sale.total_amount_cache
            else:
                tax_amount[sale.id] = sale.get_tax_amount()
                # Begin : Management of tax included
                if sale.price_type == 't_inc':
                    total_amount[sale.id]  = sum(
                        (line.amount for line in sale.lines
                            if line.type == 'line'), _ZERO)
                    untaxed_amount[sale.id] = (
                        total_amount[sale.id] - tax_amount[sale.id])
                # End : Management of tax included
                else:
                    untaxed_amount[sale.id] = sum(
                        (line.amount for line in sale.lines
                            if line.type == 'line'), _ZERO)
                    total_amount[sale.id] = (
                        untaxed_amount[sale.id] + tax_amount[sale.id])

        result = {
            'untaxed_amount': untaxed_amount,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
            }
        for key in result.keys():
            if key not in names:
                del result[key]
        return result

    def _get_invoice_sale(self, invoice_type):
        '''
        Return an updated invoice with price type
        '''
        pool = Pool()
        Invoice = pool.get('account.invoice')

        invoice = super(Sale, self)._get_invoice_sale(invoice_type)
        invoice.price_type = self.price_type
        invoice.description = self.description
        invoice.reference = self.reference
        return invoice

    @classmethod
    def create(cls, vlist):
        Party = Pool().get('party.party')
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if not user.id == 0 and not user.shop:
            cls.raise_user_error('not_sale_shop')

        for vals in vlist:
            if not 'price_type' in vals:
                price_type = Party(vals['party']).price_type or None
                if not price_type:
                    price_type = user.shop.price_type or None
                if not price_type or price_type == 'both':
                    price_type = 't_exc'
                vals['price_type'] = price_type
        return super(Sale, cls).create(vlist)

class SaleLine(ModelSQL, ModelView):
    __name__ = 'sale.line'

    amount_texc = fields.Function(fields.Numeric('Amount (tax excl.)',
        digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2)),
        states={
            'invisible': Bool(~Eval('type').in_(['line', 'subtotal'])) |
                Bool(Eval('_parent_sale', {}).get('price_type') == 't_exc'),
            },
        depends = ['type', 'quantity', 'unit_price']),
        'get_amount_texc')

    @fields.depends('_parent_sale.price_type')
    def on_change_unit(self):
        return super(SaleLine, self).on_change_unit()

    @fields.depends('_parent_sale.price_type')
    def on_change_product(self):
        res = super(SaleLine, self).on_change_product()
        res['amount_texc'] = self.on_change_with_amount_texc()
        return res

    @fields.depends('_parent_sale.price_type')
    def on_change_quantity(self):
        return super(SaleLine, self).on_change_quantity()

    @fields.depends('_parent_sale.price_type','taxes','_parent_sale.party',
        'type', 'quantity', 'unit_price', 'unit')
    def on_change_with_amount_texc(self):
        pool = Pool()
        Tax = pool.get('account.tax')

        if self.type == 'line':
            context = self.sale.get_tax_context()
            if context.get('tax_included', False):
                base = 0
                currency = self.sale.currency if self.sale else None
                with Transaction().set_context(**context):
                    taxes = Tax.compute(self.taxes,
                        self.unit_price or Decimal('0.0'),
                        self.quantity or '0.0')
                for tax in taxes:
                    base += tax['base']
                if not base:
                    # Base is alway equal to zero, there are no taxes and the
                    # get the amount
                    base = self.on_change_with_amount()
                if currency:
                    return currency.round(base)
                return base
        return Decimal('0.0')

    @fields.depends('amount')
    def get_amount_texc(self, name):
        return self.on_change_with_amount_texc()

    def _get_context_sale_price(self):
        context = super(SaleLine, self)._get_context_sale_price()
        context['tax_included'] = True if self.sale.price_type == 't_inc' \
            else False
        return context

    def get_invoice_line(self, invoice_type):
        invoice_lines = super(SaleLine, self).get_invoice_line(invoice_type)
        if invoice_lines and self.sale.price_type == 't_inc':
            invoice_lines[0].amount_texc = self.amount_texc
        return invoice_lines
