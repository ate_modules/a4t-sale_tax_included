Sale Tax Included Module
########################

This module allows the management of sales with prices including all taxes.

Product
*******

On the product you set a price all taxes included. By default the proposed 
price is the price excluding tax which are added taxes defined, but you can put
the price you want.

Shop
****

On the store you can define if it is a store allowing prices all taxes 
included only the tax price only or both

Party
*****

The same thing as the store is possible on party.

Sale
****

During the sale, if the store and the customer chose the permits, you can 
choose whether it is a sale with price included tax or duty. Once this is 
selected you can continue selling as you typically, the good prices will be
selected.

Invoice
*******

The same thing as the sale is possible on invoice.