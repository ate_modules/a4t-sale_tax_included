# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond import backend
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not
from trytond.pool import Pool, PoolMeta

__all__ = ['Template', 'Product']
__metaclass__ = PoolMeta

DEPENDS = ['active','tax_included']
_ZERO = Decimal('0.0')
STATES = {
    'readonly': ~Eval('active', True),
    }

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.INFO)


class Template(ModelSQL, ModelView):
    __name__ = "product.template"

    tax_included = fields.Boolean('Price Tax included',
        help='If checked you must define a price with tax included.')
    list_price_tinc = fields.Property(fields.Numeric('List Price (Tax inc.)',
        digits=(16, 4), depends=DEPENDS,
        states={
            'required': Bool(Eval('tax_included', False)),
            'invisible': ~Eval('tax_included', False),
            },
        ))
    current_price = fields.Function(fields.Numeric('Current Price',
        digits=(16, 4), depends=DEPENDS,
        ), 'get_current_price')


    @classmethod
    def __register__(cls, module_name):
        _logger.debug("[__register__] Begin")
        ModelField = Pool().get('ir.model.field')

        # Rename list price description
        fields = ModelField.search([
                ('name', '=', 'list_price'),
                ('field_description', '=', 'List Price'),
                ('model.model', '=', 'product.template'),
                ])
        if fields:
            _logger.debug("[__register__] Rename 'list_price'")
            ModelField.write(fields, {
                    'field_description': 'List Price (Tax excl.)',
                    })
        super(Template, cls).__register__(module_name)

    @staticmethod
    def default_tax_included():
        return False

    @staticmethod
    def default_list_price_tinc():
        return _ZERO

    def get_current_price(self, name):
        _logger.debug("[get_list_price] Begin")
        if Transaction().context.get('tax_included', False):
            return self.list_price_tinc
        return self.list_price

    @fields.depends('tax_included', 'list_price', 'list_price_tinc',
        'taxes_category', 'customer_taxes', 'category')
    def on_change_tax_included(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        Product = pool.get('product.template')
        res = {}
        context = {}

        if self.tax_included:
            if self.list_price and not self.list_price_tinc:
                res['list_price_tinc'] = self.list_price
                context.update({'tax_included': False})
                with Transaction().set_context(context):
                    taxes = Tax.compute(
                        Tax.browse(Product.get_taxes(self,
                            'customer_taxes_used')),
                        self.list_price, '1')
                for tax in taxes:
                    res['list_price_tinc'] += tax['amount']
        return res


class Product:
    __name__ = 'product.product'

    @classmethod
    def __setup__(cls):
        super(Product, cls).__setup__()
        cls._error_messages.update({
            'list_price_with_tax_included_required': (
                "This product '%(product)s' doesn't have 'List price' with tax "
                "included, you must define one for use it in a business "
                "transaction with prices with taxes included."),
            })

    # Override the default static method to get the good field for showing
    # price on list of product in sale line.
    @classmethod
    def get_price_uom(cls, products, name):
        if name[:-4] == 'list_price':
            if Transaction().context.get('tax_included', False):
                name = name.replace('list_price', 'list_price_tinc')
        return super(Product, cls).get_price_uom(products, name)

    @classmethod
    def get_sale_price(cls, products, quantity=0):
        '''
        Return the sale price for products and quantity.
        It uses if exists from the context:
            uom: the unit of measure
            currency: the currency id for the returned price
        '''
        pool = Pool()
        Uom = pool.get('product.uom')
        User = pool.get('res.user')
        Currency = pool.get('currency.currency')
        Date = pool.get('ir.date')

        today = Date.today()
        prices = {}

        uom = None
        if Transaction().context.get('uom'):
            uom = Uom(Transaction().context.get('uom'))

        currency = None
        if Transaction().context.get('currency'):
            currency = Currency(Transaction().context.get('currency'))

        user = User(Transaction().user)

        for product in products:
            if Transaction().context.get('tax_included', False):
                if product.tax_included:
                    prices[product.id] = product.list_price_tinc
                else:
                    cls.raise_user_error(
                        'list_price_with_tax_included_required', {
                        'product': product.name,
                    })
                    return {}
            else:
                prices[product.id] = product.list_price
            if uom:
                prices[product.id] = Uom.compute_price(
                    product.default_uom, prices[product.id], uom)
            if currency and user.company:
                if user.company.currency != currency:
                    date = Transaction().context.get('sale_date') or today
                    with Transaction().set_context(date=date):
                        prices[product.id] = Currency.compute(
                            user.company.currency, prices[product.id],
                            currency, round=False)
        return prices