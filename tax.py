# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import datetime
import logging
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['Tax',]

__metaclass__ = PoolMeta

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


class Tax(ModelSQL, ModelView):
    '''
    Account Tax

    Type:
        percentage:
            base = price_unit / (1 + rate)
            amount = base - price_unit
        fixed:
            base = price_unit - amount
        none: tax = none
    '''
    __name__ = 'account.tax'

    def _process_tax(self, price_unit, tax_included=False):
        '''
        Overload the standard method to add the calculation of taxes from a
        tax included price
        '''
        if not tax_included:
            return super(Tax, self)._process_tax(price_unit)

        base = price_unit
        if self.type == 'percentage':
            base = self.company.currency.round(price_unit / (1 + self.rate))
            amount = price_unit - base
        if self.type == 'fixed':
            base = price_unit - self.amount
            amount = self.amount
        return {
            'base': base,
            'amount': amount,
            'tax': self,
            }

    @classmethod
    def _unit_compute(cls, taxes, price_unit, date, tax_included=False):
        res = []

        # If the price is tax excluded : Standard treatment ...
        if not tax_included:
            _logger.debug('[_unit_compute] Standard treatment ...')
            return super(Tax, cls)._unit_compute(taxes, price_unit, date)

        # ... else ...
        _logger.debug('[_unit_compute] Tax included treatment ...')
        for tax in taxes:
            start_date = tax.start_date or datetime.date.min
            end_date = tax.end_date or datetime.date.max
            if not (start_date <= date <= end_date):
                continue
            if tax.type != 'none':
                res.append(tax._process_tax(price_unit,
                    tax_included=tax_included))
            if len(tax.childs):
                res.extend(cls._unit_compute(tax.childs, price_unit,
                    date, tax_included=tax_included))
        return res

    @classmethod
    def compute(cls, taxes, price_unit, quantity, date=None):
        pool = Pool()
        Date = pool.get('ir.date')

        tax_included = Transaction().context.get('tax_included', False)

        # If the price is tax excluded : Standard treatment ...
        if not tax_included:
            _logger.debug('[compute] Standard treatment ...')

            return super(Tax, cls).compute(taxes, price_unit, quantity,
                date=date)

        # ... else ...
        _logger.debug('[_unit_compute] Tax included treatment ...')
        if date is None:
            date = Date.today()
        taxes = cls.sort_taxes(taxes)
        quantity = Decimal(str(quantity or 0.0))
        stotal_tinc = price_unit * quantity

        return cls._unit_compute(taxes, stotal_tinc, date,
            tax_included=tax_included)