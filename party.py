# -*- coding: utf-8 -*-
# This file is part of Sale Tax Included Module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Party']

_PRICE_TYPE = [
    ('both', 'Tax included & Tax excluded'),
    ('t_inc', 'Tax included (only)'),
    ('t_exc', 'Tax excluded (only)'),
    ]

class Party(ModelSQL, ModelView):
    __name__ = 'party.party'

    price_type = fields.Selection(_PRICE_TYPE, 'Price type',
        help='Prices type for this party, with parameterization of the store '
            'he will help Tryton to selecting the default type of prices.')

    @staticmethod
    def default_price_type():
        return 'both'

